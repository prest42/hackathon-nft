# first-nft

Hello gitlab


## Getting started (mint nft)

1. cd into root of the project (hackaton-nft)
2. Install Web3: npm install @alch/alchemy-web3
3. Mint!: node scripts/mint-nft.js
    - if you get error: "Something went wrong when submitting your transaction: Error: Returned error: nonce too low"
    - increment tokenId
If everything went fine you will get something like that:
    - "The hash of your transaction is:  0x5debfa47aadbf0de8b1d04bb121d7c15c0aff38d3652b6ef046dcb8d504e7b95
    Check Alchemy's Mempool to view the status of your transaction!"

## Import NFT to Metamask
Metamask needs 2 values:
1. Smart Contract Address: https://sepolia.etherscan.io/address/0xf536511e0cf7b0dddc1fc21871aef749bbb724f3
2. TokenId: The one that you wrote in the minting
