async function main() {
  const BatteryNFT = await ethers.getContractFactory("BatteryNFT")

  // Start deployment, returning a promise that resolves to a contract object
  const batteryNFT = await BatteryNFT.deploy()
  await batteryNFT.deployed()
  console.log("Contract deployed to address:", batteryNFT.address)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })

