/**
 * * @type import('hardhat/config').HardhatUserConfig
 * */
require('dotenv').config();
require("@nomiclabs/hardhat-ethers");
// const { API_URL, PRIVATE_KEY } = process.env;
const API_URL = "https://sepolia.infura.io/v3/e22263da932241b4a652df190751bfc0"
const PRIVATE_KEY = "911510851e683067cbd3a0ed25d484dddfb829c537f272df53342098c3a04763"
module.exports = {
  solidity: "0.8.18",
  defaultNetwork: "sepolia",
  networks: {
    hardhat: {},
    sepolia: {
      url:  "https://sepolia.infura.io/v3/e22263da932241b4a652df190751bfc0",
      accounts: [`0x911510851e683067cbd3a0ed25d484dddfb829c537f272df53342098c3a04763`]
    }
  },
}

